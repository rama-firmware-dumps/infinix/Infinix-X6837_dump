#!/bin/bash

cat boot.img.* 2>/dev/null >> boot.img
rm -f boot.img.* 2>/dev/null
cat vendor_boot.img.* 2>/dev/null >> vendor_boot.img
rm -f vendor_boot.img.* 2>/dev/null
cat system/system/apex/com.android.btservices.apex.* 2>/dev/null >> system/system/apex/com.android.btservices.apex
rm -f system/system/apex/com.android.btservices.apex.* 2>/dev/null
cat system/system/app/MiraVision/MiraVision.apk.* 2>/dev/null >> system/system/app/MiraVision/MiraVision.apk
rm -f system/system/app/MiraVision/MiraVision.apk.* 2>/dev/null
cat system_ext/apex/com.android.vndk.v30.apex.* 2>/dev/null >> system_ext/apex/com.android.vndk.v30.apex
rm -f system_ext/apex/com.android.vndk.v30.apex.* 2>/dev/null
cat system_ext/app/AIVoiceAssistant/AIVoiceAssistant.apk.* 2>/dev/null >> system_ext/app/AIVoiceAssistant/AIVoiceAssistant.apk
rm -f system_ext/app/AIVoiceAssistant/AIVoiceAssistant.apk.* 2>/dev/null
cat system_ext/app/AiGallery/AiGallery.apk.* 2>/dev/null >> system_ext/app/AiGallery/AiGallery.apk
rm -f system_ext/app/AiGallery/AiGallery.apk.* 2>/dev/null
cat system_ext/app/SmartAssistant/SmartAssistant.apk.* 2>/dev/null >> system_ext/app/SmartAssistant/SmartAssistant.apk
rm -f system_ext/app/SmartAssistant/SmartAssistant.apk.* 2>/dev/null
cat system_ext/app/TranssionCamera/TranssionCamera.apk.* 2>/dev/null >> system_ext/app/TranssionCamera/TranssionCamera.apk
rm -f system_ext/app/TranssionCamera/TranssionCamera.apk.* 2>/dev/null
cat system_ext/priv-app/TranSettingsApk/TranSettingsApk.apk.* 2>/dev/null >> system_ext/priv-app/TranSettingsApk/TranSettingsApk.apk
rm -f system_ext/priv-app/TranSettingsApk/TranSettingsApk.apk.* 2>/dev/null
cat product/app/LatinImeGoogle/LatinImeGoogle.apk.* 2>/dev/null >> product/app/LatinImeGoogle/LatinImeGoogle.apk
rm -f product/app/LatinImeGoogle/LatinImeGoogle.apk.* 2>/dev/null
cat product/app/SpeechServicesByGoogle/SpeechServicesByGoogle.apk.* 2>/dev/null >> product/app/SpeechServicesByGoogle/SpeechServicesByGoogle.apk
rm -f product/app/SpeechServicesByGoogle/SpeechServicesByGoogle.apk.* 2>/dev/null
cat product/app/TrichromeLibrary/TrichromeLibrary.apk.* 2>/dev/null >> product/app/TrichromeLibrary/TrichromeLibrary.apk
rm -f product/app/TrichromeLibrary/TrichromeLibrary.apk.* 2>/dev/null
cat product/app/WebViewGoogle/WebViewGoogle.apk.* 2>/dev/null >> product/app/WebViewGoogle/WebViewGoogle.apk
rm -f product/app/WebViewGoogle/WebViewGoogle.apk.* 2>/dev/null
cat product/operator/app/HealthLife/HealthLife.apk.* 2>/dev/null >> product/operator/app/HealthLife/HealthLife.apk
rm -f product/operator/app/HealthLife/HealthLife.apk.* 2>/dev/null
cat product/priv-app/ARCore/ARCore.apk.* 2>/dev/null >> product/priv-app/ARCore/ARCore.apk
rm -f product/priv-app/ARCore/ARCore.apk.* 2>/dev/null
cat product/priv-app/FreeFire/FreeFire.apk.* 2>/dev/null >> product/priv-app/FreeFire/FreeFire.apk
rm -f product/priv-app/FreeFire/FreeFire.apk.* 2>/dev/null
cat product/priv-app/GmsCore/GmsCore.apk.* 2>/dev/null >> product/priv-app/GmsCore/GmsCore.apk
rm -f product/priv-app/GmsCore/GmsCore.apk.* 2>/dev/null
cat product/priv-app/Phonesky/Phonesky.apk.* 2>/dev/null >> product/priv-app/Phonesky/Phonesky.apk
rm -f product/priv-app/Phonesky/Phonesky.apk.* 2>/dev/null
cat product/priv-app/Velvet/Velvet.apk.* 2>/dev/null >> product/priv-app/Velvet/Velvet.apk
rm -f product/priv-app/Velvet/Velvet.apk.* 2>/dev/null
