## sys_tssi_64_armv82_infinix-user 13 TP1A.220624.014 485776 release-keys
- Transsion Name: Infinix HOT 40 Pro
- Manufacturer: infinix
- Platform: mt6789
- Codename: Infinix-X6837
- Brand: Infinix
- Flavor: sys_tssi_64_armv82_infinix-user
- Release Version: 13
- Kernel Version: 5.10.177
- Id: TP1A.220624.014
- Incremental: 231121V851
- Tags: release-keys
- CPU Abilist: arm64-v8a,armeabi-v7a,armeabi
- A/B Device: true
- Treble Device: true
- Locale: en-US
- Screen Density: 480
- Fingerprint: Infinix/X6837-OP/Infinix-X6837:13/TP1A.220624.014/231121V851:user/release-keys
- OTA version: undefined
- Branch: sys_tssi_64_armv82_infinix-user-13-TP1A.220624.014-485776-release-keys
- Repo: infinix/infinix-x6837_dump
